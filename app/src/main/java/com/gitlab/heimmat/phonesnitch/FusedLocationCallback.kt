package com.gitlab.heimmat.phonesnitch

import android.location.Location
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult

/**
 * Implementation of LocationCallback that stores the last know location for public retrieval
 * @param initialValue The initial value of the last known location, if known
 * @see LocationCallback
 */
class FusedLocationCallback(initialValue: Location? = null): LocationCallback() {
    var lastKnownLocation: Location? = initialValue
        private set(value) { field = value}

    override fun onLocationResult(p0: LocationResult?) {
        lastKnownLocation = p0?.lastLocation
    }
}