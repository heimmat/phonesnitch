package com.gitlab.heimmat.phonesnitch

import android.content.Context
import android.preference.PreferenceManager
import android.widget.Toast
import org.influxdb.dto.Query
import java.io.File
import java.lang.Exception

/**
 * The class DatabaseManager offers a simple interface to the Influx database
 * @author Matthias Heiming
 */
class DatabaseManager(private val influxConnector: InfluxConnector) {
    companion object {
        /**
         * Instantiate a new instance of DatabaseManager with connection values pulled from SharedPreferences
         * @param context The android context
         * @see InfluxConnector
         */
        fun getDefault(context: Context): DatabaseManager {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            val url = prefs.getString(context.getString(R.string.pref_url_key), "")!!
            val database = prefs.getString(context.getString(R.string.pref_database_key), context.getString(R.string.pref_database_default))!!
            val user = prefs.getString(context.getString(R.string.pref_user_key), context.getString(R.string.pref_user_default))!!
            val password = prefs.getString(context.getString(R.string.pref_password_key), "")!!
            val connector = InfluxConnector(url, user, password, database)
            return DatabaseManager(connector)
        }
    }

    private val tag = "DatabaseManager"

    /**
     * Upload a list of files that are written in Influx Line Protocol to the server
     * @param context The android context
     * @param filesToUpload A list of File objects
     */
    fun uploadFiles(context: Context, filesToUpload: List<File>) {
        Log.v(tag, "uploadFiles: ${filesToUpload.size} files to upload")
        val thread = Thread {
            Log.v(tag, "Started thread")
            try {
                val connection = influxConnector.connection
                for (file in filesToUpload) {
                    Log.v(tag, "File ${file.name}")
                    val lines = mutableListOf<String>()
                    file.forEachLine {
                        lines.add(it)
                    }
                    Log.v(tag, "${lines.size} to write")
                    connection.write(lines)
                    Log.v(tag, "Write successful")
                    val recordingManager = RecordingManager(context)
                    val oldRecording = recordingManager.get(file.name)!!
                    recordingManager.set(file.name, Recording(file.name, oldRecording.recordingIdentifier, oldRecording.uploadCount + 1))
                }
            }
            catch(e: Exception) {
                Log.e(tag, e.message, e)
                ToastHelper.show(context, "Upload failed", Toast.LENGTH_LONG)
            }

        }
        thread.start()
    }

    /**
     * Test the current connection to the database and display a toast message containing the result
     * @param context The android context
     */
    fun checkConnection(context: Context) {
        val thread = Thread {
            try {
                val connection = influxConnector.connection
                val queryRes = connection.query(Query("SHOW DATABASES"))

                if (!queryRes.hasError()) {
                    ToastHelper.show(context, "Connection Success", Toast.LENGTH_SHORT)
                }
                else {
                    ToastHelper.show(context, "Connection Error:\n${queryRes.error}", Toast.LENGTH_LONG)
                }


            }
            catch (e: Exception) {
                ToastHelper.show(context, "Connection Error:\n${e.localizedMessage}", Toast.LENGTH_LONG)
            }

        }
        thread.start()
    }


}