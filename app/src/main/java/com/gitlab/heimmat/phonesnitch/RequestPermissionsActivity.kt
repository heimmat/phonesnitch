package com.gitlab.heimmat.phonesnitch

import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import kotlinx.android.synthetic.main.activity_request_permissions.*

/**
 * Activity to display when permissions are missing
 */
class RequestPermissionsActivity : AppCompatActivity() {

    private val tag = "RequestPermissionsActivity"
    companion object {
        fun checkAllGranted(context: Context): Boolean {
            return permissions.none {
                ActivityCompat.checkSelfPermission(context, it) != PackageManager.PERMISSION_GRANTED
            }

        }

        private val permissions = arrayOf(
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            //android.Manifest.permission.ACCESS_NETWORK_STATE,
            //android.Manifest.permission.ACCESS_WIFI_STATE,
            android.Manifest.permission.FOREGROUND_SERVICE,
            android.Manifest.permission.INTERNET,
            //android.Manifest.permission.READ_PHONE_STATE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE

        )
    }

    private val reqCode = 1337



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.v(tag, "Creating activity")
        setContentView(R.layout.activity_request_permissions)

        request_permissions_fab.setOnClickListener {
            ActivityCompat.requestPermissions(this, permissions, reqCode)
        }


    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == reqCode) {
            Log.v(tag, "Received permission results")
            finish()
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
