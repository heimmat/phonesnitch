package com.gitlab.heimmat.phonesnitch

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.telephony.CellInfo
import android.telephony.CellInfoGsm
import android.telephony.CellInfoLte
import android.telephony.CellInfoWcdma
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import kotlinx.android.synthetic.main.activity_in_recording.*
import java.util.*
import kotlin.NoSuchElementException

/**
 * Activity that is displayed when device is being monitored. Initiates the Monitoring Service and listens for updates.
 *
 * @see MonitoringService
 * @see CellUpdateReceiver
 * @see LocationUpdateReceiver
 */
class InRecordingActivity : AppCompatActivity() {

    companion object {
        const val RECORDING_IDENTIFIER = "RECORDING_IDENTIFIER"
        const val SHARED_PREFS = "SHARED_PREFS"
        const val LOCATION_DESCRIPTION_KEY = "LOCATION_DESCRIPTION_KEY"
        const val DIRNAME = "PhoneSnitch/recordings"

        fun getFilename(recordingIdentifier: String): String {
            return recordingIdentifier.replace(Regex("""\W"""), "_").plus(".txt")
        }
    }
    private val recordingIdentifier: String by lazy { intent.getStringExtra(RECORDING_IDENTIFIER) ?: Date().toString() }


    private val updateInfoReceiver = CellUpdateReceiver()
    private val locationUpdateReceiver = LocationUpdateReceiver()


    private val tag = "InRecordingActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_in_recording)
        title = recordingIdentifier

        val buttonTexts = ('A'..'Z').toMutableList()
        buttonTexts.add(' ')
        val sharedPreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        sharedPreferences.edit().putString(LOCATION_DESCRIPTION_KEY, "").apply()

        gridview.adapter = LocationDescriptionAdapter(this, buttonTexts.map { it.toString() }) {
            location_description.text = (it as Button).text
        }

        stop_recording_fab.setOnClickListener {
            stopRecording()
        }

        startRecording()

    }

    override fun onBackPressed() {
        Log.v(tag, "Pressed back")
        stopRecording()
    }

    private fun startRecording() {
        Log.v(tag, "Remember recording")
        RecordingManager(this).set(getFilename(recordingIdentifier), Recording(getFilename(recordingIdentifier), recordingIdentifier))
        val intent = Intent(this, MonitoringService::class.java)
        intent.putExtra(RECORDING_IDENTIFIER, recordingIdentifier)
        Log.v(tag, "Starting service")
        startService(intent)
    }

    private fun stopRecording() {
        val intent = Intent(this, MonitoringService::class.java)
        Log.v(tag, "Stopping service")
        stopService(intent)
        val postRecordingIntent = Intent(this, PostRecordingActivity::class.java)
            .putExtra(RECORDING_IDENTIFIER, recordingIdentifier)
            .putExtra(PostRecordingActivity.FILENAME, getFilename(recordingIdentifier))
        Log.v(tag, "Start PostRecordingActivity")
        startActivity(postRecordingIntent)
    }

    override fun onStart() {
        Log.v(tag, "Activity starting, registering receivers")
        val broadcastManager = LocalBroadcastManager.getInstance(this)
        broadcastManager.registerReceiver(updateInfoReceiver, IntentFilter(MonitoringService.CELL_UPDATE))
        broadcastManager.registerReceiver(locationUpdateReceiver, IntentFilter(MonitoringService.LOCATION_UPDATE))
        super.onStart()
    }

    override fun onPause() {
        Log.v(tag, "Activity pausing, unregistering receivers")
        val broadcastManager = LocalBroadcastManager.getInstance(this)
        broadcastManager.unregisterReceiver(updateInfoReceiver)
        broadcastManager.unregisterReceiver(locationUpdateReceiver)
        super.onPause()
    }

    inner class CellUpdateReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == MonitoringService.CELL_UPDATE) {
                val bundle = intent.getBundleExtra(MonitoringService.CELL_INFO_BUNDLE)
                if (bundle != null) {
                    val cellInfo = bundle.getParcelableArray(MonitoringService.CELL_INFO)
                    if (cellInfo != null) {
                        try {
                            val firstRegisteredCell = cellInfo.first {
                                if (it is CellInfo) {
                                    it.isRegistered
                                }
                                else {
                                    false
                                }
                            }
                            if (firstRegisteredCell is CellInfo) {
                                when (firstRegisteredCell) {
                                    is CellInfoGsm -> {
                                        network_type.text = "GSM"
                                        dBm.text = firstRegisteredCell.cellSignalStrength.dbm.toString()
                                        rsrp.text = "-"
                                        rsrq.text = "-"
                                        mcc.text = if (Build.VERSION.SDK_INT >= 28) firstRegisteredCell.cellIdentity.mccString else firstRegisteredCell.cellIdentity.mcc.toString()
                                        mnc.text = if (Build.VERSION.SDK_INT >= 28) firstRegisteredCell.cellIdentity.mncString else firstRegisteredCell.cellIdentity.mnc.toString()
                                        lac.text = firstRegisteredCell.cellIdentity.lac.toString()
                                        ci.text = firstRegisteredCell.cellIdentity.cid.toString()
                                        rssnr.text = "-"
                                        pci.text = "-"
                                        earfcn.text = "-"
                                        bandwidth.text = "-"
                                        cqi.text = "-"
                                        timing_advance.text = if (Build.VERSION.SDK_INT >= 26) firstRegisteredCell.cellSignalStrength.timingAdvance.toString() else "-"
                                        psc.text = "-"
                                        uarfcn.text = "-"
                                        bsic.text = if (Build.VERSION.SDK_INT >= 24) firstRegisteredCell.cellIdentity.bsic.toString() else "-"
                                        arfcn.text = if (Build.VERSION.SDK_INT >= 24) firstRegisteredCell.cellIdentity.arfcn.toString() else "-"

                                    }
                                    is CellInfoWcdma -> {
                                        network_type.text = "WCDMA"
                                        dBm.text = firstRegisteredCell.cellSignalStrength.dbm.toString()
                                        rsrp.text = "-"
                                        rsrq.text = "-"
                                        mcc.text = if (Build.VERSION.SDK_INT >= 28) firstRegisteredCell.cellIdentity.mccString else firstRegisteredCell.cellIdentity.mcc.toString()
                                        mnc.text = if (Build.VERSION.SDK_INT >= 28) firstRegisteredCell.cellIdentity.mncString else firstRegisteredCell.cellIdentity.mnc.toString()
                                        lac.text = firstRegisteredCell.cellIdentity.lac.toString()
                                        ci.text = firstRegisteredCell.cellIdentity.cid.toString()
                                        rssnr.text = "-"
                                        pci.text = "-"
                                        earfcn.text = "-"
                                        bandwidth.text = "-"
                                        cqi.text = "-"
                                        timing_advance.text = "-"
                                        psc.text = firstRegisteredCell.cellIdentity.psc.toString()
                                        uarfcn.text = if (Build.VERSION.SDK_INT >= 24) firstRegisteredCell.cellIdentity.uarfcn.toString() else "-"
                                        bsic.text = "-"
                                        arfcn.text = "-"

                                    }
                                    is CellInfoLte -> {
                                        network_type.text = "LTE"
                                        dBm.text = firstRegisteredCell.cellSignalStrength.dbm.toString()
                                        rsrp.text = if (Build.VERSION.SDK_INT >= 26) firstRegisteredCell.cellSignalStrength.rsrp.toString() else "-"
                                        rsrq.text = if (Build.VERSION.SDK_INT >= 26) firstRegisteredCell.cellSignalStrength.rsrq.toString() else "-"
                                        mcc.text = if (Build.VERSION.SDK_INT >= 28) firstRegisteredCell.cellIdentity.mccString else firstRegisteredCell.cellIdentity.mcc.toString()
                                        mnc.text = if (Build.VERSION.SDK_INT >= 28) firstRegisteredCell.cellIdentity.mncString else firstRegisteredCell.cellIdentity.mnc.toString()
                                        lac.text = firstRegisteredCell.cellIdentity.tac.toString()
                                        ci.text = firstRegisteredCell.cellIdentity.ci.toString()
                                        rssnr.text = if (Build.VERSION.SDK_INT >= 26) firstRegisteredCell.cellSignalStrength.rssnr.toString() else "-"
                                        pci.text = firstRegisteredCell.cellIdentity.pci.toString()
                                        earfcn.text = if (Build.VERSION.SDK_INT >= 24) firstRegisteredCell.cellIdentity.earfcn.toString() else "-"
                                        bandwidth.text = if (Build.VERSION.SDK_INT >= 28) firstRegisteredCell.cellIdentity.bandwidth.toString() else "-"
                                        cqi.text = if (Build.VERSION.SDK_INT >= 26) firstRegisteredCell.cellSignalStrength.cqi.toString() else "-"
                                        timing_advance.text = firstRegisteredCell.cellSignalStrength.timingAdvance.toString()
                                        psc.text = "-"
                                        uarfcn.text = "-"
                                        bsic.text = "-"
                                        arfcn.text = "-"

                                    }
                                    else -> {
                                        network_type.text = "Other"
                                        dBm.text = "-"
                                        rsrp.text = "-"
                                        rsrq.text = "-"
                                        mcc.text = "-"
                                        mnc.text = "-"
                                        lac.text = "-"
                                        ci.text = "-"
                                        rssnr.text = "-"
                                        pci.text = "-"
                                        earfcn.text = "-"
                                        bandwidth.text = "-"
                                        cqi.text = "-"
                                        timing_advance.text = "-"
                                        psc.text = "-"
                                        uarfcn.text = "-"
                                        bsic.text = "-"
                                        arfcn.text = "-"


                                    }
                                }
                            }
                        } catch (e: NoSuchElementException) {
                            Log.e(tag, e.message, e)
                        }
                    }

                }
            }
        }
    }

    inner class LocationUpdateReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == MonitoringService.LOCATION_UPDATE) {
                val loc = intent.getParcelableExtra<Location>(MonitoringService.LOCATION)
                longitude.text = loc.longitude.toString()
                latitude.text = loc.latitude.toString()
            }
        }
    }


}
