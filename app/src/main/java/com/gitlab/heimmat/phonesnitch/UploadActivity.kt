package com.gitlab.heimmat.phonesnitch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.CheckBox
import kotlinx.android.synthetic.main.activity_upload.*
import kotlinx.android.synthetic.main.activity_upload.nav_bar
import kotlinx.android.synthetic.main.recording.view.*
import java.io.File

/**
 * Activity to upload measurement files
 */
class UploadActivity : AppCompatActivity() {

    private val tag = "UploadActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload)

        nav_bar.replaceMenu(R.menu.activity_upload)
        nav_bar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.delete_item -> {
                    deleteFiles()
                    finish()
                }
                R.id.select_all_item -> {
                    setSelectionForAll(true)
                }
                R.id.deselect_all_item -> {
                    setSelectionForAll(false)
                }
            }
            true
        }


        val dir = ExternalWriter().getDir(InRecordingActivity.DIRNAME)
        val files = mutableListOf<File>()
        if (dir != null) {
            files.addAll(dir.listFiles())
        }

        val records = files.map {
            RecordingManager(this).get(it.name)
        }

        gridview.adapter = RecordingAdapter(this, records.filterNotNull())
        gridview.setOnItemClickListener { parent, view, position, id ->
            val selectedCheckBox: CheckBox = view.findViewById(R.id.selected)
            selectedCheckBox.isChecked = !selectedCheckBox.isChecked
        }

        upload_recording_fab.setOnClickListener {
            uploadFiles()
        }
    }

    private fun uploadFiles() {
        val filesToUpload = getSelectedFiles()
        Log.v(tag, filesToUpload.toString())
        val databaseManager = DatabaseManager.getDefault(this)
        databaseManager.uploadFiles(this, filesToUpload)
        finish()

    }

    private fun deleteFiles() {
        val filesToDelete = getSelectedFiles()
        for (file in filesToDelete) {
            Log.v(tag, "Deleting file ${file.name}")
            file.delete()
        }
        finish()
    }

    private fun getSelectedFiles(): List<File> {
        val selectedFiles = mutableListOf<File>()
        for (iterator in 0 until gridview.childCount) {
            val view = gridview.getChildAt(iterator)
            if (view.findViewById<CheckBox>(R.id.selected).isChecked) {
                val file = ExternalWriter().getFile(view.filename.text.toString(), InRecordingActivity.DIRNAME)
                if (file != null) {
                    selectedFiles.add(file)
                }
            }
        }
        return selectedFiles
    }

    private fun setSelectionForAll(selected: Boolean) {
        for (iterator in 0 until gridview.childCount) {
            val view = gridview.getChildAt(iterator)
            view.findViewById<CheckBox>(R.id.selected).isChecked = selected
        }
    }
}
