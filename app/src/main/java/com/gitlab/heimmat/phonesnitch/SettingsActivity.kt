package com.gitlab.heimmat.phonesnitch

import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {

    private val tag = "SettingsActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.v(tag, "Creating activity")
        setContentView(R.layout.activity_settings)

        check_connection_fab.setOnClickListener {
            checkConnection()
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        return if (canNavigateBack) {
            super.onSupportNavigateUp()
        } else {
            Log.v(tag, "Stopped up navigation because of implausible settings")
            false
        }
    }

    private fun checkConnection() {
        val databaseManager = DatabaseManager.getDefault(this)
        databaseManager.checkConnection(this)
    }

    override fun onBackPressed() {
        if (canNavigateBack) {
            super.onBackPressed()
        }
        else {
            Log.v(tag, "Stopped back navigation because of implausible settings")
        }
    }

    private val canNavigateBack: Boolean get() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val interval = prefs.getString(getString(R.string.pref_interval_key), getString(R.string.pref_interval_default))
        val intervalNumber = interval?.toIntOrNull()
        val isNull = intervalNumber != null
        if (!isNull) {
            Toast.makeText(this, R.string.not_valid_number, Toast.LENGTH_LONG).show()
        }

        return isNull

    }
}
