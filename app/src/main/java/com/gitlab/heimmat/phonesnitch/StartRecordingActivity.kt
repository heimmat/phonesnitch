package com.gitlab.heimmat.phonesnitch

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_start_recording.*

/**
 * Activity that is displayed when recording is being initiated to let user submit a recording identifier
 */
class StartRecordingActivity : AppCompatActivity() {

    private val tag = "StartRecordingActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.v(tag, "Creating activity")
        setContentView(R.layout.activity_start_recording)
        start_recording_fab.setOnClickListener {
            startRecording()
        }
        if (!RequestPermissionsActivity.checkAllGranted(this)) {
            val intent = Intent(this, RequestPermissionsActivity::class.java)
            startActivity(intent)
        }
    }

    private fun startRecording() {
        val recordName = recording_identifier.text.toString()
        val intent = Intent(this, InRecordingActivity::class.java)
            .putExtra(InRecordingActivity.RECORDING_IDENTIFIER, recordName)
        startActivity(intent)
    }
}
