package com.gitlab.heimmat.phonesnitch

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button

/**
 * Adapter to display buttons defined via buttonNames
 * @param buttonNames The list of button names
 * @param context The execution contex
 */
class LocationDescriptionAdapter(private val context: Context, val buttonNames: List<String>, private val onClickListener: ((View) -> Unit)?): BaseAdapter() {

    override fun getCount(): Int {
        return buttonNames.size
    }

    override fun getItem(position: Int): String {
        return buttonNames[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var viewToReturn = convertView

        if (convertView == null) {
            val layoutInflater = LayoutInflater.from(context)
            viewToReturn = layoutInflater.inflate(R.layout.location_description_button, null)
            (viewToReturn as Button).text = getItem(position)
            viewToReturn.setOnClickListener {
                val button = it as Button
                val locationDescription = button.text.toString().trim()
                val sharedPreferences = context.getSharedPreferences(InRecordingActivity.SHARED_PREFS, Context.MODE_PRIVATE)
                sharedPreferences.edit().putString(InRecordingActivity.LOCATION_DESCRIPTION_KEY, locationDescription).apply()
                onClickListener?.invoke(it)
            }
        }

        return viewToReturn!!
    }
}