package com.gitlab.heimmat.phonesnitch

import android.content.Context
import android.content.SharedPreferences
import java.util.*

/**
 * Retrieves or creates a unique identifier for this device which is stored in SharedPreferences
 * @param context The execution context
 *
 * @see SharedPreferences
 */
class DeviceIdentifier(private val context: Context) {

    private val tag = "DeviceIdentifier"
    private val PREF_NAME = "DEVICE_IDENTIFIER_PREF"
    private val UUID_KEY = "UUID_KEY"
    private val prefs: SharedPreferences get() = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

    /**
     * Gets a unique string representation of a UUID
     */
    fun get(): String {
        val uuidString = prefs.getString(UUID_KEY, "")
        if (uuidString != null && uuidString != "") {
            return uuidString
        }
        else {
            val uuid = UUID.randomUUID()
            set(uuid)
            return uuid.toString()
        }
    }

    private fun set(value: UUID) {
        prefs.edit().putString(UUID_KEY, value.toString()).apply()
    }
}