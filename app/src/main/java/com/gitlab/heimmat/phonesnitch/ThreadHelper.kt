package com.gitlab.heimmat.phonesnitch

import android.os.Handler
import android.os.Looper

class ThreadHelper {
    companion object {
        fun runOnMainThread(function: () -> Unit) {
            val handler = Handler(Looper.getMainLooper())
            handler.post(function)
        }
    }

}
