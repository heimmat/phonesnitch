package com.gitlab.heimmat.phonesnitch

import android.content.Context
import com.google.gson.GsonBuilder

/**
 * Manager to retrieve from and write to SharedPreferences
 * @param context The execution context
 */
class RecordingManager(context: Context) {
    companion object {
        private const val RECORD_STORE = "RECORD_STORE"
    }

    private val sharedPrefs = context.getSharedPreferences(RECORD_STORE, Context.MODE_PRIVATE)
    private val gson = GsonBuilder()
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        .create()

    /**
     * Get a recording using its filename as a key
     * @param filename The recording's filename
     * @return The recording, null if not found
     */
    fun get(filename: String): Recording? {
        val storedRecording = sharedPrefs.getString(filename, null)
        return if (storedRecording != null) {
            gson.fromJson(storedRecording, Recording::class.java) as Recording
        } else {
            null
        }
    }

    /**
     * Write a recording to SharedPreferences using the filename as key
     * @param filename SharedPreferences Key
     * @param recording The recording to store
     */
    fun set(filename: String, recording: Recording) {
        sharedPrefs.edit().putString(filename, gson.toJson(recording)).apply()
    }


}