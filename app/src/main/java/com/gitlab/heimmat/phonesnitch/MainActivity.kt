package com.gitlab.heimmat.phonesnitch

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val tag = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.v(tag, "Creating activity")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nav_bar.replaceMenu(R.menu.activity_main)
        nav_bar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.request_permissions_item -> {
                    requestPermissions()
                }
                R.id.upload_recordings_item -> {
                    uploadFiles()
                }
                R.id.settings_item -> {
                    goToSettings()
                }
            }
            true
        }
        start_recording_fab.setOnClickListener {
            startRecording()
        }
        if (settingsNeeded) {
            Log.v(tag, "Not all settings configured. Go to settings activity")
            goToSettings()
        }

        device_id.text = getString(R.string.device_identifier, DeviceIdentifier(this).get())
    }

    private fun uploadFiles() {
        val intent = Intent(this, UploadActivity::class.java)
        startActivity(intent)
    }

    private fun startRecording() {
        val intent = Intent(this, StartRecordingActivity::class.java)
        startActivity(intent)
    }

    private fun requestPermissions() {
        val intent = Intent(this, RequestPermissionsActivity::class.java)
        startActivity(intent)
    }

    private fun goToSettings() {
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
    }

    private val settingsNeeded: Boolean get() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val storedKey = prefs.getString(getString(R.string.pref_password_key), "")
        val storedUrl = prefs.getString(getString(R.string.pref_url_key), "")
        return storedKey == "" || storedUrl == ""
    }


}
