package com.gitlab.heimmat.phonesnitch

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView

/**
 * Adapter to display a list of recordings as cards
 */
class RecordingAdapter(private val context: Context, private val recordings: List<Recording>): BaseAdapter() {
    override fun getCount(): Int {
        return recordings.size
    }

    override fun getItem(position: Int): Recording {
        return recordings[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val recording = getItem(position)
        val file = ExternalWriter().getFile(recording.filename, InRecordingActivity.DIRNAME)

        var viewToReturn = convertView

        if (convertView == null) {
            val layoutInflater = LayoutInflater.from(context)
            viewToReturn = layoutInflater.inflate(R.layout.recording, null)
        }

        val filenameTextView = viewToReturn!!.findViewById<TextView>(R.id.filename)
        val selectedCheckBox = viewToReturn.findViewById<CheckBox>(R.id.selected)
        val uploadIndicator: ImageView = viewToReturn.findViewById(R.id.upload_indicator)
        val fileInfoTextView: TextView = viewToReturn.findViewById(R.id.file_info)
        val recordingIdTextView: TextView = viewToReturn.findViewById(R.id.recording_identifier)
        filenameTextView.text = recording.filename
        recordingIdTextView.text = recording.recordingIdentifier
        uploadIndicator.visibility = if (recording.uploadCount > 0) View.VISIBLE else View.INVISIBLE
        selectedCheckBox.isChecked = recording.uploadCount == 0
        if (file != null) {
            fileInfoTextView.text = android.text.format.Formatter.formatFileSize(context, file.length())
        }

        return viewToReturn
    }
}