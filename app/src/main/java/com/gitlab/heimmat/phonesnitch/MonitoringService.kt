package com.gitlab.heimmat.phonesnitch

import android.Manifest
import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.Looper
import android.preference.PreferenceManager
import android.telephony.*
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.location.*
import org.influxdb.dto.Point
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Workhorse of the app. Requests location and cell info updates periodically
 */
class MonitoringService : Service() {

    companion object {
        const val CELL_UPDATE = "CELL_UPDATE"
        const val CELL_INFO = "CELL_INFO"
        const val CELL_INFO_BUNDLE = "CELL_INFO_BUNDLE"
        const val LOCATION_UPDATE = "LOCATION_UPDATE"
        const val LOCATION = "LOCATION"
    }

    private val tag = "MonitoringService"

    private val cellListener = CellListener(::handleCellInfo)
    private val telephonyManager: TelephonyManager by lazy { getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager }
    private lateinit var recordingIdentifier: String // initialized in onStartCommand

    private val fusedLocationClient: FusedLocationProviderClient by lazy { LocationServices.getFusedLocationProviderClient(this) }
    private lateinit var fusedLocationCallback: FusedLocationCallback

    private val locationForwarder= object: LocationCallback() {
        override fun onLocationResult(p0: LocationResult?) {
            if (p0?.lastLocation != null) {
                forwardToConsumers(p0.lastLocation)
            }
        }

        fun forwardToConsumers(loc: Location) {
            val lbm = LocalBroadcastManager.getInstance(this@MonitoringService)
            val intent = Intent().setAction(LOCATION_UPDATE)
            intent.putExtra(LOCATION, loc)
            lbm.sendBroadcast(intent)
        }
    }

    private val timer = Timer()
    private val timerTask = object: TimerTask() {
        override fun run() {
            try {
                handleCellInfo(telephonyManager.allCellInfo)
            } catch (e: SecurityException) {
                die(R.string.missing_permissions)
            }

        }
    }

    private val updateInterval: Long get() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val interval = prefs.getString(getString(R.string.pref_interval_key), getString(R.string.pref_interval_default))!!.toLong()
        return interval * 1000
    }



    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.v(tag, "Received start command")
        recordingIdentifier = intent?.getStringExtra(InRecordingActivity.RECORDING_IDENTIFIER) ?: Date().toString()

        getInitialGpsOrDieAndThen {
            Log.v(tag, "Got initial location")
            startGpsMonitoringOrDie()
            timer.scheduleAtFixedRate(timerTask, 0, updateInterval)
            startCellMonitoringOrDie()
            startForeground(
                tag.hashCode(),
                NotificationCompat.Builder(
                    this,
                    createNotificationChannel(this::getPackageName.toString(), tag)
                )
                    .setContentTitle(getString(R.string.monitoring_service_title))
                    .setContentText(getString(R.string.monitoring_service_text))
                    .setSmallIcon(R.drawable.cctv)
                    .build())

        }

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        Log.v(tag, "Destroying service")
        telephonyManager.listen(cellListener, PhoneStateListener.LISTEN_NONE)
        fusedLocationClient.removeLocationUpdates(fusedLocationCallback)
        fusedLocationClient.removeLocationUpdates(locationForwarder)
        timer.cancel()
        stopForeground(true)
        super.onDestroy()
    }

    private fun createNotificationChannel(channelId: String, channelName: String): String {
        return if (Build.VERSION.SDK_INT >= 26) {
            val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_LOW)
            channel.lightColor = Color.DKGRAY
            channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
            val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            service.createNotificationChannel(channel)
            channelId
        } else {
            ""
        }
    }

    private fun getInitialGpsOrDieAndThen(handleIt: ((Location?) -> Unit)) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.lastLocation.addOnSuccessListener {
                fusedLocationCallback = FusedLocationCallback(it)
                handleIt(it)
            }
        }
        else {
            die(R.string.missing_permissions)
        }
    }

    private fun startGpsMonitoringOrDie() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            /*
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                updateInterval,
                0f,
                locationListener,
                Looper.getMainLooper()
            )
             */
            val request = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(updateInterval)
            fusedLocationClient.requestLocationUpdates(request, fusedLocationCallback, Looper.getMainLooper())
            fusedLocationClient.requestLocationUpdates(request, locationForwarder, Looper.getMainLooper())


        }
        else {
            die(R.string.missing_permissions)
        }
    }

    private fun startCellMonitoringOrDie() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            telephonyManager.listen(cellListener, PhoneStateListener.LISTEN_CELL_INFO)
        }
        else {
            die(R.string.missing_permissions)
        }
    }

    private fun die(messageId: Int) {
        die(getString(messageId))
    }

    private fun die(message: String) {
        Log.e(tag, "Dying: $message")
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        stopSelf()
    }

    fun handleCellInfo(cellInfo: List<CellInfo>) {
        try {
            val points = convertToPoints(cellInfo)
            writeToDisk(points)
            forwardToConsumers(cellInfo)
        } catch (e: Exception) {
            Log.e(tag, e.message, e)
            throw(e)
        }


    }

    private fun forwardToConsumers(cellInfo: List<CellInfo>) {
        val lbm = LocalBroadcastManager.getInstance(this)
        val bundle = Bundle()
        bundle.putParcelableArray(CELL_INFO, cellInfo.toTypedArray())
        val intent = Intent().putExtra(CELL_INFO_BUNDLE, bundle).setAction(CELL_UPDATE)
        lbm.sendBroadcast(intent)
    }

    private fun convertToPoints(cellInfo: List<CellInfo>): List<Point> {
        val listOfPoints = mutableListOf<Point>()
        for (cell in cellInfo) {
            when (cell) {
                is CellInfoLte -> { //LTE
                    listOfPoints.add(ltePoint(cell))
                }
                is CellInfoGsm -> { //GSM
                    listOfPoints.add(gsmPoint(cell))
                }
                is CellInfoWcdma -> { //UMTS
                    listOfPoints.add(wcdmaPoint(cell))
                }
                else -> {
                    //ignore
                }
            }
        }
        return listOfPoints
    }

    private fun writeToDisk(points: List<Point>) {
        for (point in points) {
            ExternalWriter().appendFile("${point.lineProtocol()}\n", InRecordingActivity.getFilename(recordingIdentifier), InRecordingActivity.DIRNAME)
        }
    }

    private fun initializeDefaultPointBuilder(cell: CellInfo): Point.Builder {

        val systemInfoCollector = SystemInfoCollector(this)


        return Point.measurement("CellInfo")
            .time(Date().time, TimeUnit.MILLISECONDS)
            .tag("isRegistered", cell.isRegistered.toString())
            .tag("cellConnectionStatus", cell.cellConnectionStatusString)
            .tag("recId", recordingIdentifier)
            .tag("locationDescription", systemInfoCollector.locationDescription)
            .tag("model", systemInfoCollector.model)
            .tag("manufacturer", systemInfoCollector.manufacturer)
            .tag("os", systemInfoCollector.versionName)
            .tag("carrier", systemInfoCollector.provider)
            .tag("deviceId", DeviceIdentifier(this).get())
            .addNullableField("latitude", fusedLocationCallback.lastKnownLocation?.latitude)
            .addNullableField("longitude", fusedLocationCallback.lastKnownLocation?.longitude)
            .addNullableField("altitude", fusedLocationCallback.lastKnownLocation?.altitude)
    }

    private val CellInfo.cellConnectionStatusString : String get() {
        return if (Build.VERSION.SDK_INT >= 28) {
            when (this.cellConnectionStatus) {
                CellInfo.CONNECTION_NONE -> {
                    "CONNECTION_NONE"
                }
                CellInfo.CONNECTION_PRIMARY_SERVING -> {
                    "CONNECTION_PRIMARY_SERVING"
                }
                CellInfo.CONNECTION_SECONDARY_SERVING -> {
                    "CONNECTION_SECONDARY_SERVING"
                }
                else -> {
                    "CONNECTION_UNKNOWN"
                }
            }
        }
        else {
            "CONNECTION_UNKNOWN"
        }
    }

    @TargetApi(28)
    private fun ltePoint(cell: CellInfoLte): Point {
        val pointBuilder = initializeDefaultPointBuilder(cell)
            .tag("cellType", "LTE")
            .addField("timingAdvance", cell.cellSignalStrength.timingAdvance)
            .tag("cid", cell.cellIdentity.ci.toString())
            .tag("lac", cell.cellIdentity.tac.toString())
            .addField("pci", cell.cellIdentity.pci)
            .addField("dbm", cell.cellSignalStrength.dbm)
        if (ge24) {
            pointBuilder.addField("earfcn", cell.cellIdentity.earfcn)
        }

        if (ge26) {
            pointBuilder.addField("rsrp", cell.cellSignalStrength.rsrp)
                .addField("rsrq", cell.cellSignalStrength.rsrq)
                .addField("rssnr", cell.cellSignalStrength.rssnr)
                .addField("cqi", cell.cellSignalStrength.cqi)
        }
        if (ge28) {
            pointBuilder.nullableTag("mnc", cell.cellIdentity.mncString)
                .nullableTag("mcc", cell.cellIdentity.mccString)
                .addField("bandwidth", cell.cellIdentity.bandwidth)
        }
        else {
            pointBuilder.tag("mnc", cell.cellIdentity.mnc.toString())
                .tag("mcc", cell.cellIdentity.mcc.toString())
        }
        return pointBuilder.build()
    }

    @TargetApi(28)
    private fun gsmPoint(cell: CellInfoGsm): Point {
        val pointBuilder = initializeDefaultPointBuilder(cell)
            .tag("cellType", "GSM")
            .tag("cid", cell.cellIdentity.cid.toString())
            .tag("lac", cell.cellIdentity.lac.toString())
            .addField("dbm", cell.cellSignalStrength.dbm)

        if (ge24) {
            pointBuilder.addField("arfcn", cell.cellIdentity.arfcn)
                .addField("bsic", cell.cellIdentity.bsic)
        }
        if (ge26) {
            pointBuilder.addField("timingAdvance", cell.cellSignalStrength.timingAdvance)
        }
        if (ge28) {
            pointBuilder.nullableTag("mnc", cell.cellIdentity.mncString)
                .nullableTag("mcc", cell.cellIdentity.mccString)
        }
        else {
            pointBuilder.tag("mnc", cell.cellIdentity.mnc.toString())
                .tag("mcc", cell.cellIdentity.mcc.toString())
        }
        return pointBuilder.build()
    }

    @TargetApi(28)
    private fun wcdmaPoint(cell: CellInfoWcdma): Point {
        val pointBuilder = initializeDefaultPointBuilder(cell)
            .tag("cellType", "WCDMA")
            .addField("dbm", cell.cellSignalStrength.dbm)
            .tag("cid", cell.cellIdentity.cid.toString())
            .tag("lac", cell.cellIdentity.lac.toString())
            .addField("psc", cell.cellIdentity.psc)

        if (ge24) {
            pointBuilder.addField("uarfcn", cell.cellIdentity.uarfcn)
        }

        if (ge28) {
            pointBuilder.nullableTag("mnc", cell.cellIdentity.mncString)
                .nullableTag("mcc", cell.cellIdentity.mccString)
        }
        else {
            pointBuilder.tag("mnc", cell.cellIdentity.mnc.toString())
                .tag("mcc", cell.cellIdentity.mcc.toString())
        }
        return pointBuilder.build()
    }

    private val ge24 = Build.VERSION.SDK_INT >= 24
    private val ge26 = Build.VERSION.SDK_INT >= 26
    private val ge28 = Build.VERSION.SDK_INT >= 28

}


