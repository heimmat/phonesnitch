package com.gitlab.heimmat.phonesnitch

import android.os.Environment
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * Helper class for interfacing the Android External Storage API
 */
class ExternalWriter {
    private val tag = "ExternalWriter"

    /**
     * Checks if external storage is available for read and write
     */
    private fun isExternalStorageWritable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }

    /**
     *  Checks if external storage is available to at least read
     */
    private fun isExternalStorageReadable(): Boolean {
        return Environment.getExternalStorageState() in
                setOf(Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY)
    }

    /**
     * Get a File object representing a subdirectory of the Documents directory
     * @param dirName A string representing a directory name or path
     */
    private fun getPublicDocumentDir(dirName: String): File {
        val file = File(Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOCUMENTS), dirName)
        if (!file.mkdirs() && !file.exists()) {
            android.util.Log.e(tag, "Directory not created")
        }
        return file
    }

    /**
     * Append a file with a new line of the given message
     * @param message The message to append
     * @param fileName A string representing the file name
     * @param dirName A string representing a subfolder of the Documents directory
     */
    fun appendFile(message: String, fileName: String, dirName: String) {
        if (isExternalStorageWritable()) {
            val file = File(getPublicDocumentDir(dirName), fileName)
            val stream = FileOutputStream(file, true)
            try {
                stream.write(message.toByteArray())
            } catch (e: IOException) {
                android.util.Log.e(tag, e.message)
            }
            finally {
                stream.close()
            }
        }
        else {
            android.util.Log.e(tag, "Can't write to file")
        }


    }

    /**
     * Retrieve a file
     * @param fileName The file's name
     * @param dirName A string representing a subfolder of the Documents directory
     * @return A file object or null when file doesn't exist or storage is not readable
     */
    fun getFile(fileName: String, dirName: String): File? {
        return if (isExternalStorageReadable()) {
            val file = File(getPublicDocumentDir(dirName), fileName)
            if (file.exists()) {
                file
            } else {
                null
            }
        } else {
            null
        }
    }

    /**
     * Retrieve a directory
     * @param dirName A string representing a subfolder of the Documents directory
     * @return A File object or null if storage is not readable or there is no directory with that name
     */
    fun getDir(dirName: String): File? {
        return if (isExternalStorageReadable()) {
            val dir = getPublicDocumentDir(dirName)
            if (dir.isDirectory) {
                dir
            } else {
                null
            }
        } else {
            null
        }
    }


}
