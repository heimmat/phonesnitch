package com.gitlab.heimmat.phonesnitch

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_post_recording.*

/**
 * Activity that is displayed after a recording is completed
 */
class PostRecordingActivity : AppCompatActivity() {

    companion object {
        const val FILENAME = "FILENAME"
    }

    private val tag = "PostRecordingActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.v(tag, "Creating activity")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_recording)

        val filename: String= intent.getStringExtra(FILENAME)
        val recording = RecordingManager(this).get(filename)
        title = recording?.recordingIdentifier

        adapterview.isEnabled = false
        adapterview.adapter = RecordingAdapter(this, listOfNotNull(recording))

        upload_recording_fab.setOnClickListener {
            uploadRecording()
        }
    }

    override fun onBackPressed() {
        Log.v(tag, "Back pressed")
        backToMain()
    }

    private fun backToMain() {
        val intent = Intent(this, MainActivity::class.java)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    private fun uploadRecording() {
        val databaseManager = DatabaseManager.getDefault(this)
        databaseManager.uploadFiles(this,
            listOfNotNull(ExternalWriter().getFile(intent.getStringExtra(FILENAME), InRecordingActivity.DIRNAME))
        )
        backToMain()
    }
}
