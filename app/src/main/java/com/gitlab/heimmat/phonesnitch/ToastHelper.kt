package com.gitlab.heimmat.phonesnitch

import android.content.Context
import android.widget.Toast



class ToastHelper {

    companion object {
        fun show(context: Context, message: CharSequence, duration: Int) {
            /*
            val handler = Handler(Looper.getMainLooper())
            handler.post {
                Toast.makeText(context, message, duration).show()
            }
            */
            ThreadHelper.runOnMainThread { Toast.makeText(context, message, duration).show() }
        }

        fun show(context: Context, resId: Int, duration: Int) {
            /*
            val handler = Handler(Looper.getMainLooper())
            handler.post {
                Toast.makeText(context, resId, duration).show()
            }
            */
            ThreadHelper.runOnMainThread { Toast.makeText(context, resId, duration).show() }
        }
    }
}

