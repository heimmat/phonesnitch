package com.gitlab.heimmat.phonesnitch

import okhttp3.OkHttpClient
import org.influxdb.BatchOptions
import org.influxdb.InfluxDB
import org.influxdb.InfluxDBFactory
import java.util.concurrent.TimeUnit

/**
 * Wrapper for the InfluxDB with default settings
 * @see InfluxDB
 * @param url A string representing a URL to the Influx Server
 * @param user A string representing a username with write permissions on the database
 * @param password A string representing the user's password
 * @param database The database to use
 * @property connection The database connection for further use
 */
class InfluxConnector(url: String, user: String, password: String, database: String) {

    private val okHttpBuilder = OkHttpClient.Builder()
        .writeTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .callTimeout(60, TimeUnit.SECONDS)

    val connection: InfluxDB by lazy {
       InfluxDBFactory.connect(url, user, password, okHttpBuilder)
            .setDatabase(database)
            .enableBatch(BatchOptions.DEFAULTS.actions(2000).flushDuration(100))
            .setRetentionPolicy("autogen")
            .enableGzip()

    }


}
