package com.gitlab.heimmat.phonesnitch

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.telephony.TelephonyManager
import androidx.core.app.ActivityCompat

class SystemInfoCollector(private val context: Context) {
    private val telephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    private val canReadPhoneState: Boolean get() = ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED

    val imei: String
        get() {
            return if (canReadPhoneState) {
                if (Build.VERSION.SDK_INT >= 26) {
                    telephonyManager.imei
                } else {
                    telephonyManager.deviceId
                }
            } else {
                ""
            }
        }

    val phoneNumber: String
        get() {
            if (canReadPhoneState) {
                return telephonyManager.line1Number
            } else {
                return ""
            }
        }

    val provider: String get() = telephonyManager.networkOperatorName

    val manufacturer: String get() = Build.MANUFACTURER

    val model: String get() = Build.MODEL

    val versionCode: Int get() = Build.VERSION.SDK_INT

    val versionName: String get() = Build.VERSION.RELEASE

    val imsi: String get() {
        return if (canReadPhoneState) {
            telephonyManager.subscriberId
        }
        else {
            ""
        }
    }

    val locationDescription: String get() {
        val prefs = context.getSharedPreferences(InRecordingActivity.SHARED_PREFS, Context.MODE_PRIVATE)
        val locDesc = prefs.getString(InRecordingActivity.LOCATION_DESCRIPTION_KEY, "")
        return locDesc ?: ""
    }

}
