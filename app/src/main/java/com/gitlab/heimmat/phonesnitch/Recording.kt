package com.gitlab.heimmat.phonesnitch

class Recording(val filename: String, val recordingIdentifier: String, val uploadCount: Int = 0)