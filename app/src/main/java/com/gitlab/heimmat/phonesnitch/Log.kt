package com.gitlab.heimmat.phonesnitch

import java.io.FileNotFoundException
import java.text.SimpleDateFormat
import java.util.*


class Log {
    companion object {
        fun v(tag: String, msg: String, throwable: Throwable? = null): Int {
            try {
                appendLogFile(LogLevel.VERBOSE, tag, "Message: $msg${if (throwable == null) "" else ", throwable: $throwable"}")

            } catch (e: FileNotFoundException) {
                android.util.Log.e(tag, "Cannot write to file. Check if permission was granted")
            }
            return if (throwable == null) {
                android.util.Log.v(tag, msg)
            }
            else {
                android.util.Log.v(tag, msg, throwable)
            }
        }

        fun w(tag: String, msg: String, throwable: Throwable? = null): Int {
            try {
                appendLogFile(LogLevel.WARN, tag, "Message: $msg${if (throwable == null) "" else ", throwable: $throwable"}")

            } catch (e: FileNotFoundException) {
                android.util.Log.e(tag, "Cannot write to file. Check if permission was granted")
            }
            return if (throwable == null) {
                android.util.Log.w(tag, msg)
            }
            else {
                android.util.Log.w(tag, msg, throwable)
            }
        }

        fun i(tag: String, msg: String, throwable: Throwable? = null): Int {
            try {
                appendLogFile(LogLevel.INFO, tag, "Message: $msg${if (throwable == null) "" else ", throwable: $throwable"}")

            } catch (e: FileNotFoundException) {
                android.util.Log.e(tag, "Cannot write to file. Check if permission was granted")
            }
            return if (throwable == null) {
                android.util.Log.i(tag, msg)
            }
            else {
                android.util.Log.i(tag, msg, throwable)
            }
        }

        fun e(tag: String, msg: String?, throwable: Throwable? = null): Int {
            try {
                appendLogFile(LogLevel.ERROR, tag, "Message: $msg${if (throwable == null) "" else ", throwable: $throwable"}")

            } catch (e: FileNotFoundException) {
                android.util.Log.e(tag, "Cannot write to file. Check if permission was granted")
            }
            return if (throwable == null) {
                android.util.Log.e(tag, msg)
            }
            else {
                android.util.Log.e(tag, msg, throwable)
            }
        }

        fun d(tag: String, msg: String, throwable: Throwable? = null): Int {
            try {
                appendLogFile(LogLevel.DEBUG, tag, "Message: $msg${if (throwable == null) "" else ", throwable: $throwable"}")

            } catch (e: FileNotFoundException) {
                android.util.Log.e(tag, "Cannot write to file. Check if permission was granted")
            }
            return if (throwable == null) {
                android.util.Log.d(tag, msg)
            }
            else {
                android.util.Log.d(tag, msg, throwable)
            }
        }

        private fun shortLogLevel(level: LogLevel): String {
            return when (level) {
                LogLevel.VERBOSE -> "V"
                LogLevel.INFO -> "I"
                LogLevel.DEBUG -> "D"
                LogLevel.WARN -> "W"
                LogLevel.ERROR -> "E"
            }
        }

        private fun appendLogFile(level: LogLevel, tag: String, message: String) = ExternalWriter().appendFile("${Date()} [${shortLogLevel(level)}/$tag]: $message\n", "log_${SimpleDateFormat("yyyy-MM-dd").format(Date())}", "PhoneSnitch")

    }



    private enum class LogLevel {
        VERBOSE,
        INFO,
        DEBUG,
        WARN,
        ERROR
    }
}
