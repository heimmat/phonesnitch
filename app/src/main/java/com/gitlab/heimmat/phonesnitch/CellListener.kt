package com.gitlab.heimmat.phonesnitch

import android.telephony.CellInfo
import android.telephony.PhoneStateListener

/**
 * Listener for PhoneStateListener.LISTEN_CELL_INFO. Forwards the received list of CellInfo to a specified handler
 * @param cellInfoHandler The handler
 * @see PhoneStateListener.LISTEN_CELL_INFO
 * @see CellInfo
 */
class CellListener(private val cellInfoHandler: (List<CellInfo>) -> Unit): PhoneStateListener() {

    private val tag = "CellListener"

    override fun onCellInfoChanged(cellInfo: MutableList<CellInfo>?) {
        if (cellInfo != null) {
            cellInfoHandler.invoke(cellInfo)
        }
        else {
            Log.v(tag, "Received null")
        }

        super.onCellInfoChanged(cellInfo)
    }


}