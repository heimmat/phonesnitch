package com.gitlab.heimmat.phonesnitch

import org.influxdb.dto.Point

fun Point.Builder.addNullableField(field: String, value: Boolean?): Point.Builder {
    return if (value != null) {
        addField(field, value)
    }
    else {
        this
    }
}

fun Point.Builder.addNullableField(field: String, value: Long?): Point.Builder {
    return if (value != null) {
        addField(field, value)
    }
    else {
        this
    }
}

fun Point.Builder.addNullableField(field: String, value: Double?): Point.Builder {
    return if (value != null) {
        addField(field, value)
    }
    else {
        this
    }
}

fun Point.Builder.addNullableField(field: String, value: Number?): Point.Builder {
    return if (value != null) {
        addField(field, value)
    }
    else {
        this
    }
}

fun Point.Builder.addNullableField(field: String, value: String?): Point.Builder {
    return if (value != null) {
        addField(field, value)
    }
    else {
        this
    }
}

fun Point.Builder.nullableTag(tagName: String, value: String?): Point.Builder {
    return if (value != null) {
        tag(tagName, value)
    }
    else {
        this
    }
}
