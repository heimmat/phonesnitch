# PhoneSnitch
PhoneSnitch is an easy-to-use App for Android to collect cell measurements with consumer devices.
Data is written to local memory and can be uploaded to an InfluxDB-Server.

A receiving InfluxDB-Server including a Grafana frontend can be set up using the [PhoneSnitch-Server](https://gitlab.com/heimmat/phonesnitch-server).

## Building
1. Clone this repo and open in IntelliJ IDEA or Android-Studio. 
2. Build the gradle project.

## Configuring
Start the app and configure your settings by clicking the gear icon. Make sure you set the settings exactly like you configured your instance of the [PhoneSnitch-Server](https://gitlab.com/heimmat/phonesnitch-server).